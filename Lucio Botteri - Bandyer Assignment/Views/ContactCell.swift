//
//  ContactCell.swift
//  Lucio Botteri - Bandyer Assignment
//
//  Created by Lucio Botteri on 19/10/2020.
//

import UIKit

class ContactCell: UITableViewCell {
    
    var user: User?

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        accessoryType = selected ? .checkmark : .none
    }
    
    func configure(with user: User) {
        self.user = user
        textLabel?.text = user.name + " " + user.surname
    }
    
    func errorAnimation() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.error)
        backgroundColor = .systemRed
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut) {
            self.backgroundColor = .clear
        }
    }
}
