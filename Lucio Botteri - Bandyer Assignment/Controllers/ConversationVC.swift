//
//  ConversationVC.swift
//  Lucio Botteri - Bandyer Assignment
//
//  Created by Lucio Botteri on 20/10/2020.
//

import UIKit
import AVFoundation

class ConversationVC: UIViewController {
    
    @IBOutlet weak private var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak private var previewView: UIView!
    
    @IBOutlet weak private var contact1: UIView!
    @IBOutlet weak private var contact2: UIView!
    @IBOutlet weak private var contact3: UIView!
    @IBOutlet weak private var contact4: UIView!
    @IBOutlet var labels: [UILabel]!
    
    private var videoDataOutput: AVCaptureVideoDataOutput!
    private var videoDataOutputQueue: DispatchQueue!
    private var previewLayer: AVCaptureVideoPreviewLayer!
    private var captureDevice: AVCaptureDevice!
    private let session = AVCaptureSession()
    
    private var timer: Timer!
    private var timerCount = 3 {
        didSet {
            if timerCount < 1 {
                if bottomConstraint.constant > -140 {
                    bottomConstraint.constant = -140
                    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut) {
                        [weak self] in
                        self?.view.layoutIfNeeded()
                    }
                }
                timerCount = 0
                labels.forEach {
                    $0.isHidden = true
                }
            } else {
                labels.forEach {
                    $0.isHidden = false
                }
            }
        }
    }
    var contacts = [User]()
    
    lazy var frontInput: AVCaptureDeviceInput? = {
        var frontDevice: AVCaptureDevice?
        for device in AVCaptureDevice.devices(for: .video) {
            if device.position == .front {
                frontDevice = device
            }
        }
        if let frontDevice = frontDevice {
            return try? AVCaptureDeviceInput(device: frontDevice)
        }
        return nil
    }()
    
    lazy var backInput: AVCaptureDeviceInput? = {
        var backDevice: AVCaptureDevice?
        for device in AVCaptureDevice.devices(for: .video) {
            if device.position == .back {
                backDevice = device
            }
        }
        if let backDevice = backDevice {
            return try? AVCaptureDeviceInput(device: backDevice)
        }
        return nil
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        switch contacts.count {
        case 1:
            contact2.isHidden = true
            fallthrough
        case 2:
            contact3.isHidden = true
            contact3.superview?.isHidden = true
            fallthrough
        case 3:
            contact4.isHidden = true
        default:break
        }
        
        for (i,contact) in contacts.enumerated() {
            let view: UIView
            switch i {
            case 0:
                view = contact1
            case 1:
                view = contact2
            case 2:
                view = contact3
            default:
                view = contact4
            }
            if let imageView = view.subviews.first(where: { $0 is UIImageView }) as? UIImageView {
                imageView.image = contact.image
            }
            if let label = view.subviews.first(where: { $0 is UILabel }) as? UILabel {
                label.text = contact.name + " " + contact.surname
            }
        }
        
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {
            [weak self] _ in
            self?.timerCount -= 1
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        previewView.contentMode = .scaleAspectFit
        setupAVCapture()
    }
    
    @IBAction func endCallAction(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func flipCamera(_ sender: UIButton) {
        timerCount = 3
        if let frontDeviceInput = frontInput {
            session.beginConfiguration()
            
            if let currentInput = session.inputs.first as? AVCaptureDeviceInput {
                session.removeInput(currentInput)
                
                if let newDeviceInput = (currentInput.device.position == .front) ? backInput : frontDeviceInput {
                    session.addInput(newDeviceInput)
                }
            }
            session.commitConfiguration()
        }
    }
    
    @IBAction func showButtonsAction(_ sender: UIButton) {
        if bottomConstraint.constant < 0 {
            timerCount = 3
            bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut) {
                [weak self] in
                self?.view.layoutIfNeeded()
            }
        } else {
            timerCount = 0
        }
    }
}


// AVCaptureVideoDataOutputSampleBufferDelegate protocol and related methods
extension ConversationVC: AVCaptureVideoDataOutputSampleBufferDelegate{
    func setupAVCapture() {
        session.sessionPreset = .vga640x480
        guard let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else { return }
        captureDevice = device
        beginSession()
    }
    
    func beginSession() {
        var deviceInput: AVCaptureDeviceInput!
        
        do {
            deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            guard deviceInput != nil else {
                print("error: can't get deviceInput")
                return
            }
            
            if session.canAddInput(deviceInput) {
                session.addInput(deviceInput)
            }
            
            videoDataOutput = AVCaptureVideoDataOutput()
            videoDataOutput.alwaysDiscardsLateVideoFrames = true
            videoDataOutputQueue = DispatchQueue(label: "VideoDataOutputQueue")
            videoDataOutput.setSampleBufferDelegate(self, queue: videoDataOutputQueue)
            
            if session.canAddOutput(videoDataOutput) {
                session.addOutput(videoDataOutput)
            }
            
            videoDataOutput.connection(with: .video)?.isEnabled = true
            
            previewLayer = AVCaptureVideoPreviewLayer(session: session)
            previewLayer.videoGravity = .resizeAspectFill
            
            let rootLayer = previewView.layer
            rootLayer.masksToBounds = true
            previewLayer.frame = rootLayer.bounds
            rootLayer.addSublayer(previewLayer)
            session.startRunning()
        } catch {
            deviceInput = nil
            print("error: \(error.localizedDescription)")
        }
    }
}
