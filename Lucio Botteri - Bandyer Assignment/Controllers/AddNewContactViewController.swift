//
//  AddNewContactViewController.swift
//  Lucio Botteri - Bandyer Assignment
//
//  Created by Lucio Botteri on 18/10/2020.
//

import UIKit
import Contacts

class AddNewContactViewController: UIViewController {
    
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var surnameTextField: UITextField!
    
    var delegate: ContactsVCDelegate?
    
    @IBAction private func closeAction(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction private func doneAction(_ sender: UIButton) {
        guard let name = nameTextField.text, let surname = surnameTextField.text, name.count > 0, surname.count > 0 else {
            return
        }
        AppData.shared.contacts.append(User(name: name.trimmingCharacters(in: .whitespacesAndNewlines), surname: surname.trimmingCharacters(in: .whitespacesAndNewlines)))
        delegate?.reloadData()
        dismiss(animated: true)
    }
}

extension AddNewContactViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            surnameTextField.becomeFirstResponder()
        } else {
            textField.endEditing(true)
        }
        return true
    }
}
