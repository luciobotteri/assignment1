//
//  ViewController.swift
//  Lucio Botteri - Bandyer Assignment
//
//  Created by Lucio Botteri on 18/10/2020.
//

import UIKit
import SwiftKeychainWrapper

class LoginViewController: UIViewController {
    
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var rememberMeSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rememberMeSwitch.isOn = KeychainWrapper.standard.hasValue(forKey: "UserCredentials")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if KeychainWrapper.standard.hasValue(forKey: "UserCredentials") {
            performSegue(withIdentifier: "toMainVC", sender: self)
        }
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        login()
    }
    
    private func login() {
        guard let username = usernameTextField.text, let password = passwordTextField.text, username.count > 0, password.count > 0 else {
            let alert = UIAlertController(title: "Error", message: "Please provide a valid username and password", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default))
            present(alert, animated: true)
            return
        }
        
        if rememberMeSwitch.isOn {
            KeychainWrapper.standard.set(username+password, forKey: "UserCredentials")
        } else {
            KeychainWrapper.standard.remove(forKey: "UserCredentials")
        }
        
        performSegue(withIdentifier: "toMainVC", sender: self)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            login()
        }
        return true
    }
}
