//
//  ContactsViewController.swift
//  Lucio Botteri - Bandyer Assignment
//
//  Created by Lucio Botteri on 18/10/2020.
//

import UIKit
import Contacts
import SwiftKeychainWrapper

protocol ContactsVCDelegate: class {
    func reloadData()
}

class ContactsViewController: UIViewController, ContactsVCDelegate {
    
    @IBOutlet private weak var addButton: UIBarButtonItem!
    @IBOutlet private weak var callButton: UIButton!
    @IBOutlet private weak var contactsTableView: UITableView!
    
    var contactsToCall = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshCallButton()
        let addFromVC = UIAction(title: "Add a new contact", image: UIImage(systemName: "person.fill.badge.plus"), identifier: nil, discoverabilityTitle: nil, attributes: [], state: .off, handler: { _ in
            self.performSegue(withIdentifier: "toAddVC", sender: self)
        })
        
        let logout = UIAction(title: "Logout", image: nil, identifier: nil, discoverabilityTitle: nil, attributes: [.destructive], state: .off) { _ in
            KeychainWrapper.standard.remove(forKey: "UserCredentials")
            self.dismiss(animated: true)
        }
        
        let actions = [addFromVC, logout]
        
        let menu = UIMenu(title: "", image: nil, identifier: nil, options: .displayInline, children: actions)
        
        addButton.primaryAction = nil
        addButton.menu = menu
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let addVC = segue.destination as? AddNewContactViewController {
            addVC.delegate = self
        } else if let conversationVC = segue.destination as? ConversationVC {
            conversationVC.contacts = contactsToCall
        }
    }
    
    @IBAction func callAction(_ sender: UIButton) {
        performSegue(withIdentifier: "toConversation", sender: self)
    }
    
    func reloadData() {
        contactsToCall.removeAll()
        contactsTableView.reloadData()
    }
    
    func refreshCallButton() {
        if contactsToCall.isEmpty {
            callButton.setTitle("Select up to 4 contacts", for: .normal)
            callButton.isEnabled = false
            callButton.backgroundColor = .systemGray
        } else {
            callButton.setTitle("Call (\(contactsToCall.count))", for: .normal)
            callButton.isEnabled = true
            callButton.backgroundColor = .systemGreen
        }
    }
}

extension ContactsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppData.shared.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactCell
        cell.configure(with: AppData.shared.contacts[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            AppData.shared.contacts.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard contactsToCall.count < 4 else {
            (tableView.cellForRow(at: indexPath) as? ContactCell)?.errorAnimation()
            tableView.deselectRow(at: indexPath, animated: false)
            return
        }
        guard let user = (tableView.cellForRow(at: indexPath) as? ContactCell)?.user else { return }
        contactsToCall.append(user)
        refreshCallButton()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let user = (tableView.cellForRow(at: indexPath) as? ContactCell)?.user else { return }
        contactsToCall.removeAll { $0 == user }
        refreshCallButton()
    }
}
