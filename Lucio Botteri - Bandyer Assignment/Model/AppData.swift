//
//  AppData.swift
//  Lucio Botteri - Bandyer Assignment
//
//  Created by Lucio Botteri on 18/10/2020.
//

import Foundation

final class AppData {
    static let shared = AppData()
    
    private init() {}
    
    var contacts = [
        User(name: "John", surname: "Appleseed"),
        User(name: "Lucio", surname: "Botteri"),
        User(name: "Courtney", surname: "Love"),
        User(name: "David", surname: "Lynch"),
        User(name: "Steven Patrick", surname: "Morrissey"),
        User(name: "Laura", surname: "Shine")
    ]
}
