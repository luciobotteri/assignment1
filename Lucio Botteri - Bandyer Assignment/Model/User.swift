//
//  User.swift
//  Lucio Botteri - Bandyer Assignment
//
//  Created by Lucio Botteri on 18/10/2020.
//

import UIKit

class User {
    let name: String
    let surname: String
    var image: UIImage {
        return UIImage(named: surname) ?? UIImage(named: "user-placeholder") ?? UIImage()
    }
    
    init(name: String, surname: String) {
        self.name = name
        self.surname = surname
    }
}

extension User: Equatable {
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.name == rhs.name && lhs.surname == rhs.surname
    }
}
